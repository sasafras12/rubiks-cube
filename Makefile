# change appliaction name here (executable output name)
TARGET=main

# compiler
CC=gcc
# debug
DEBUG=-g
# optimisation
OPT=-O2
# warnings
WARN=-Wall

CCFLAGS=$(DEBUG) $(OPT) $(WARN) -pipe

GTKLIB=`pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

PNGLIB=-lpng

MATHLIB=-lm

# linker
LD=g++
LDFLAGS=$(PTHREAD) $(GTKLIB) $(PNGLIB) $(MATHLIB) -export-dynamic

OBJS=	main.o

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)

main.o: src/main.cpp
	$(CC) -c $(CCFLAGS) src/main.cpp $(GTKLIB) -o main.o

clean:
	rm -f *.o $(TARGET) *.out
